

This tutorial is dedicated to the basics of programming. In this tutorial, we will discover the main principles of programming with R. We will cover the following points:
- what is a variable
- what are a vector, a list and a dataframe
- what are functions and R packages
- how to read a file

The tutorial is written in [Markdown mark up language](https://www.markdownguide.org/).


# Using the tutorial

## Download this repository

You can do this with either git clone https://gitlab.com/d8096/health_data_science_tutorials at the command line or by downloading this repostiory as a Zip file.

## Open Jupyter Lab 

Open the Notebook of your choice. All paths are relative paths.

# Contributing

Feel free to submit an issue or to launch a pull request.

