

- [ ] Create a variable
- [ ] Perform operations between variables
- [ ] Create a vector, a dataframe, a list
- [ ] Handle the main R functions
- [ ] Read a file