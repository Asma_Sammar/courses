#########################################
#
# Exercice : Manipulation des fichiers INSEE + vote + vaccination
# Objetifs :
# - data management
# TODO : 
#
# Date de création : kddkd
# Date de dernière modification :
# Auteur : 
#
#########################################

## Chargement des bibliothèques
library(dplyr)
library(stringr)
library(readxl)

## Chemins
path_raw_data = "/media/ant/data_crypt/bases_nationales/ressources/data"
path_export_data = "/media/ant/data_crypt/01_projets/bases_nationales/votes_vaccination/data"

list.files(path_raw_data)
list.files(path_export_data)

# Variables
codes_lyon = c("69381", "69382", "69383", "69384", "69385", "69386", "69387", "69388", "69389")
codes_marseille = c("13201", "13202", "13203", "13204", "13205", "13206", "13207", "13208", "132019",
                    "13210", "13211", "13212", "13213", "13214", "13215", "13216")
codes_paris = c("75101", "75102", "75103", "75104", "75105", "75106", "75107", "75108", "75109", "75110",
                "75111", "75112", "75113", "75114", "75115", "75116", "75117", "75118", "75119", "75120")
codes_postaux_arrondissements = c(codes_lyon, codes_marseille, codes_paris)

## Notes

# 20 juin 2021 pour le 1er tour
# 27 juin 2021 pour le second tour

# 1) Chargement des données
##########################################################################
# https://gitlab.univ-lille.fr/master_dss/datalib/

# Vaccination
# chargement du fichier avec read.csv2

# vaccination_src = read.csv2(file.path(path_raw_data, "donnees-de-vaccination-par-epci.csv"), 
#                             stringsAsFactors = FALSE, 
#                             dec = ".")

  # Votes
# resultats-par-commune-correctif_2021.csv

# Population
# BTT_TD_POP1B_2017.csv

# Pauvreté
# cc_filosofi_2017_DEP.csv

# RSA
# RSACOM2017.csv

# Diplômes
# base-cc-diplomes-formation-2017.csv

# CSP  
# TCRD_005.xlsx
#csp_src = readxl::read_xlsx(file.path(path_raw_data, "TCRD_005.xlsx"), sheet = 'DEP')

# Correspondances communes 2021
# correspondance_intercommunalite_codegeo_01012021.csv'

# Correspondances communes 2017 
# correspondance_intercommunalite_codegeo_2017_01012021.csv

# 2) Datamanagement
##########################################################################

# Vaccination
# ----------------------------------------------------------------------

# Filtre sur 'TOUT_AGE', semaines 24 et 25
# Sélection des colonnes semaine_injection, epci, libelle_epci, population_carto, 
# effectif_cumu_1_inj, effectif_cumu_termine, taux_cumu_1_inj, taux_cumu_termine
# Calcul du taux cumulé 1 injection, taux cumulé 2 injections
# 1 dataframe 1er tour
# 1 dataframe 2ème tour

# Votes
# ----------------------------------------------------------------------

# Sélection : region_code, region_name, commune_code, commune_name, num_tour, 
# inscrits_nb, abstention_nb, abstention_pourc)

# Filtrer AR en 6 et 7 ème lettre du code commune
# Si nombre de caractères = 4, rajouter un 0

# Merge avec correspondance_communes_epci
# regroupement par EPCI
# Taux

# Population
# ----------------------------------------------------------------------
# Retirer codegeo 13055, 69123, 75056
# Calculer le département, somme de l'âge
# Regrouper par code geo pour calculer :
# - nb_habitants
# - nb_hommes
# - nb_femmes
# - somme_age
# Fusion avec correspondance epci
# Regroupement par epci pour :
# - nb_habitants
# - somme_age
# - nb_hommes
# - nb_femmes
# - taux_hommes_vivants
# - age_moyen_vivant

# Pauvreté
# ----------------------------------------------------------------------

# RSA
# ----------------------------------------------------------------------

# Pour Lyon, Marseille, Paris, les données de RSA sont documentées par arrondissement (sans le total pour la ville)
# Il faut donc regrouper tous les arrondissements de ces trois villes

# Renommer nb_allocataires en nb_caf, et nb_allocataire_rsa en nb_rsa, communes en commune_rsa
# Sélectionner nb_caf, nb_rsa, codes_insee, commune_rsa

# Créer un dataframe sans arrondissement :
# filtrer les lignes pour lesquelles commune_rsa ne contient pas "ARRONDISSEMENT"

# Créer un dataframe pour Paris
# sélectionner les lignes dont codes_insee est présent dans les codes de Paris (variable codes_paris)
# Modifier le code_insee en "75056"
# Regrouper par code_insee (pour n'avoir qu'une seule ligne pour Paris)
# Somme pour nb_caf et nb_rsa
# commune_rsa = "Paris"

# Créer un dataframe pour Lyon
# sélectionner les lignes dont codes_insee est présent dans les codes de Lyon (variable codes_lyon)
# Modifier le code_insee en "69123"
# Regrouper par code_insee (pour n'avoir qu'une seule ligne pour Lyon)
# Somme pour nb_caf et nb_rsa
# commune_rsa = "Lyon"

# Créer un dataframe pour Marseille
# sélectionner les lignes dont codes_insee est présent dans les codes de Marseille (variable codes_marseille)
# Modifier le code_insee en "13055"
# Regrouper par code_insee (pour n'avoir qu'une seule ligne pour Marseille)
# Somme pour nb_caf et nb_rsa
# commune_rsa = "Marseille"

# Fusionner les 4 dataframes "sans arrondissement", Paris, Lyon, Marseille
# Merge avec la correspondance communes EPCI
# Faire la somme nb_caf et nb_rsa par EPCI

# Sortir les communes pour lesquelles il n'y a pas eu de correspondance avec EPCIAAAAAEE

# Diplôme
# ----------------------------------------------------------------------

# CSP
# ----------------------------------------------------------------------

# Join
# ----------------------------------------------------------------------