

from pymongo import MongoClient
import pprint

client = MongoClient()

# The above code will connect on the default host and port. We can also specify the host and port explicitly, as follows:

client = MongoClient('localhost', 27017)

# Select database and collection

db = client.hopital
patient = db.patient

# Count documents in a collection

nb_patients = patient.count_documents({})
print(nb_patients)

# Insert a document

nouveau_patient = {"patient_id": "100",
	         "first_name": "first_name_100",
	         "last_name": "last_name_100",
	         "age": 24,
	         "sexe": "H"}

print("Insert")

#patient_id = patient.insert_one(nouveau_patient)
#print(patient_id.inserted_id)

# Find a document

print(db.patient.find({"patient_id": 1}))