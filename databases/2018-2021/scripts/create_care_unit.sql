
-- UNIT

CREATE TABLE CARE_UNIT (
--
UNIT_ID		 		INT 			NOT NULL AUTO_INCREMENT,
UNIT_CODE			CHAR(4) 		NOT NULL,
UNIT_LABEL			VARCHAR(50)		NOT NULL,
DEPARTMENT_ID		INT             NOT NULL,
--
PRIMARY KEY (UNIT_ID),
FOREIGN KEY (DEPARTMENT_ID) REFERENCES DEPARTMENT (DEPARTMENT_ID)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;
