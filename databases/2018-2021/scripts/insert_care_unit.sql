
-- Insert unit

INSERT INTO `CARE_UNIT`(`UNIT_CODE`, `UNIT_LABEL`, `DEPARTMENT_ID`) 
VALUES ('U0A1','Unit A1', 1),
('U0A2','Unit A2', 1),
('U0A3','Unit A3', 1),
('U0A4','Unit A4', 1),
('U0A5','Unit A5', 1),
('U0B1','Unit B1', 2),
('U0B2','Unit B2', 2),
('U0B3','Unit B3', 2),
('U0B4','Unit B4', 2),
('U0B5','Unit B5', 2),
('U0C1','Unit C1', 3),
('U0C2','Unit C2', 3),
('U0C3','Unit C3', 3),
('U0C4','Unit C4', 3),
('U0C5','Unit C5', 3),
('U0D1','Unit D1', 4),
('U0D2','Unit D2', 4),
('U0D3','Unit D3', 4),
('U0D4','Unit D4', 4),
('U0D5','Unit D5', 4),
('U0E1','Unit E1', 5),
('U0E2','Unit E2', 5),
('U0E3','Unit E3', 5),
('U0E4','Unit E4', 5),
('U0E5','Unit E5', 5);
