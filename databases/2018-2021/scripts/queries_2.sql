
-- Select
-------------------------------------------------------------

-- select unit stay in unit U0B1


-- select unit stay in units U0A2 or U0A3


-- select unit stay in department A



-- Computation of new fields
-- -------------------------------------------------------------


-- compute the duration of each unit stay


-- compute the age at the admission date (of the stay)


-- compute the average age at the admission date (of the stay)



-- left outer join
-------------------------------------------------------------

-- number of unit_stay per unit (0 for unit without unit_stay)


-- number of unit_stay per department (0 for department without unit_stay)



-- advanced sql
-------------------------------------------------------------


-- select the stays with unit_stay in U0A1


-- count the stays with unit_stay in U0A1




-- select the stays without a unit stay in the unit U0B1


-- select the stays without a unit stay in the unit U0B1 and with a unit stay in the unit U0A1